package com.prac.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prac.auth.domain.User;
import com.prac.auth.exception.RecordNotFoundException;
import com.prac.auth.service.UserService;

@RestController
@RequestMapping(value={"/rest/user_exp"})
public class EmployeeRESTController {
	/*//
	 * http://localhost:9009/rest/user_exp/employees   -- post
	 * {
	"id":10,
    "country": "UKExp",
    "name": "testexp"
}*/
	//http://localhost:9009/rest/user_exp/employees/23 --get
	
	//http://localhost:9009/rest/user_exp/ 
	//invalid throws the
	@Autowired
	UserService userService;
	@PostMapping(value = "/employees")
	public ResponseEntity<User> addEmployee (@RequestBody User employee)
	{
		userService.createUser(employee);
	    return new ResponseEntity<User>(employee, HttpStatus.OK);
	}
	 
	@GetMapping(value = "/employees/{id}") 
	public ResponseEntity<User> getEmployeeById (@PathVariable("id") int id) throws RecordNotFoundException
	{
		User employee = userService.findById(id);
	     
	    if(employee == null) {
	         throw new RecordNotFoundException("Invalid employee id : " + id);
	    }
	    return new ResponseEntity<User>(employee, HttpStatus.OK);
	}
}
