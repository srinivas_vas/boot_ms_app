package com.prac.auth.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prac.auth.domain.Product;
import com.prac.auth.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	public List<Product> listAll() {
		return productRepository.findAll();
	}
	public void save(Product product){
		productRepository.save(product);
	}
	
	public Product get(long id){
		return productRepository.findById(id).get();
	}
	
	public void delete(long id) {
		productRepository.deleteById(id);
	}
}