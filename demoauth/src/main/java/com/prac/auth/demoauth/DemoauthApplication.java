package com.prac.auth.demoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan("com.prac.auth")
@EntityScan("com.prac.auth.domain")
@EnableJpaRepositories("com.prac.auth.repository")
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class DemoauthApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(DemoauthApplication.class, args);
		/*@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
		@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })*/
	}

}
